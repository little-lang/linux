#pragma once
#include <iostream>
#include<vector>
#include<unordered_map>
#include<string>

using namespace std;

typedef void(*func)();//定义一个函数指针类型

unordered_map<int,string> desc_task;//<task_num,task_name>任务描述表，将任务序号与任务作用联系起来的表

vector<func> task_tb;//任务表

void task1()
{
    cout << "执行任务1" << endl;
}

void task2()
{
    cout << "执行任务2" << endl;
}

void task3()
{
    cout << "执行任务3" << endl;
}

void task4()
{
    cout << "执行任务4" << endl;
}

void task5()
{
    cout << "执行任务5" << endl;
}

void load()//任务列表，可以通过这个列表将任务加载进来
{
    desc_task.insert(make_pair(task_tb.size(),"任务1"));//描述任务
    task_tb.push_back(task1);//将任务载入任务表
    
    desc_task.insert(make_pair(task_tb.size(),"任务2"));//描述任务
    task_tb.push_back(task2);//将任务载入任务表

    desc_task.insert(make_pair(task_tb.size(),"任务3"));//描述任务
    task_tb.push_back(task3);//将任务载入任务表

    desc_task.insert(make_pair(task_tb.size(),"任务4"));//描述任务
    task_tb.push_back(task4);//将任务载入任务表

    desc_task.insert(make_pair(task_tb.size(),"任务5"));//描述任务
    task_tb.push_back(task5);//将任务载入任务表

}

void showTask_tb()//展示任务表
{
    cout<<"展示任务表"<<endl;
    for(const auto &it : task_tb)
    {
        it();
    }
}

void showTask_desc()//输出任务描述表
{
    for(const auto &it : desc_task)
    {
        cout<<it.first<<": "<<it.second<<endl;
    }
}
