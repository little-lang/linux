#include "com.hpp"
#include <sys/ipc.h>
#include <sys/types.h>
#include <cassert>
#include <sys/stat.h>
#include <sys/shm.h>
#include <unistd.h>

int main()
{
    // 1.创建我们的key值给shm
    key_t key = ftok(PATH_NAME, PROJ_ID);
    assert(key != -1);
    log(DEBUG, "client成功获得共享内存的key值 | step1");

    // 2.利用shmget获取共享内存
    umask(0);
    int shmid = shmget(key, SHM_SIZE, 0);
    assert(shmid != -1);
    log(DEBUG, "client成功获取共享内存 | step2");
    cout << endl;

    // 3.使用shmat链接共享内存和进程地址空间
    char *shmaddr = (char *)shmat(shmid, nullptr, 0);
    assert(shmaddr);
    log(DEBUG, "client成功链接共享内存与进程 | step3");
    cout << endl;

    // 4.用户端进行操作
    log(DEBUG, "client用户端进行操作 | step4");
    // 操作
    sleep(10);
    cout << endl;

    // 5.使用shamdt将当前共享内存脱离进程地址空间
    int ret_dt = shmdt(shmaddr);
    assert(ret_dt != -1);
    log(DEBUG, "client成功使进程脱离共享内存 | step5");
    cout << endl;

    // 用户端不需要关闭共享内存

    return 0;
}