#include "com.hpp"
#include <sys/ipc.h>
#include <sys/types.h>
#include <cassert>
#include <sys/stat.h>
#include <sys/shm.h>
#include <unistd.h>

int main()
{
    // 1.创建我们的key值给shm
    key_t key = ftok(PATH_NAME, PROJ_ID);
    assert(key != -1);
    log(DEBUG, "server成功获得共享内存的key值 | step1");

    // 2.利用shmget创建出共享内存
    umask(0);
    int shmid = shmget(key, SHM_SIZE, IPC_CREAT | IPC_EXCL | 0666);
    assert(shmid != -1);
    log(DEBUG, "server成功创建共享内存 | step2");
    cout << endl;

    // 3.使用shmat链接共享内存和进程地址空间
    char *shmaddr = (char *)shmat(shmid, nullptr, 0);
    assert(shmaddr);
    log(DEBUG, "server成功链接共享内存与进程 | step3");
    cout << endl;

    // 4.服务端进行操作
    log(DEBUG, "server服务端进行操作 | step4");
    // 操作
    sleep(10);
    cout << endl;

    // 5.使用shamdt将当前共享内存脱离进程地址空间
    int ret_dt = shmdt(shmaddr);
    assert(ret_dt != -1);
    log(DEBUG, "server成功使进程脱离共享内存 | step5");
    cout << endl;

    // 6.使用shmctl删除创建的共享内存
    int ret_ctl = shmctl(shmid, IPC_RMID, nullptr);
    assert(ret_ctl != -1);
    log(DEBUG, "server成功删除共享内存 | step6");

    return 0;
}