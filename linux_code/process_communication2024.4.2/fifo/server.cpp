#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include "com.hpp"
#include <cassert>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#define P_NUM 5

using namespace std;

void getmessage(int fd, char *buffer)
{
    int size = read(fd, buffer, sizeof(buffer) - 1);
    assert(size != -1);
    if (size == 0)
    {
        cout <<"["<<getpid()<< "]quit " << endl;
        exit(0);
    }
    else if (size > 0)
    {
        buffer[size] = '\0';
        cout << "[" << getpid() << "] get message : " << buffer << endl;
    }
}

int main()
{
    // 打开管道
    int fd = open(pathName.c_str(), O_RDONLY);
    assert(fd != -1);
    log(DEBUG, "client管道打开成功 | step1");

    // 接收数据由进程池接收数据
    for (int i=0; i < P_NUM; i++)
    {
        pid_t pid = fork();
        if (pid == 0) // 子进程
        {
            char buffer[1024] = {0};
            while (true)
            {
                getmessage(fd, buffer);
            }
        }
    }

    for (int i=0; i < P_NUM; i++)
    {
        waitpid(-1, nullptr, 0);
    }

    // 关闭管道
    close(fd);
    log(DEBUG, "client管道关闭成功");
}