#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include "com.hpp"
#include <cassert>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>

using namespace std;

int main()
{
    int ret = mkfifo(pathName.c_str(), MODE);
    assert(ret != -1);
    log(DEBUG, "server管道创建成功 | step1");

    // 打开以写方式打开管道
    int fd = open(pathName.c_str(), O_WRONLY);
    assert(fd != -1);
    log(DEBUG, "server管道打开成功 | step2");

    // 发送数据
    char buffer[1024];
    while (true)
    {
        cout << "plase sent massage" << endl;
        int size = read(0, buffer, sizeof(buffer - 1));
        buffer[size - 1] = '\0';
        if (strcmp("quit", buffer) == 0)
            break;
        write(fd, buffer, strlen(buffer));
    }

    // 关闭管道并删除
    close(fd);
    unlink(pathName.c_str());
    log(DEBUG, "server管道删除成功");
    return 0;
}