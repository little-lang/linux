#include <iostream>
#include "threadpool.hpp"
#include "caculate_task.hpp"
#include <ctime>

using namespace std;

int main()
{
    srand((unsigned int)time(nullptr));
    char opr[] = "+-*/%";
    // 启动线程池,线程池等待任务产生处理任务
    threadPool<caculate_task>::getThreadPool()->start();
    int count = 0;
    while (1)
    {
        sleep(1);
        // 主线程生产任务
        int x, y;
        x = rand() % 10;
        usleep(10);
        y = rand() % 10;
        char op = opr[rand() % 5];
        caculate_task task(x, op, y);
        threadPool<caculate_task>::getThreadPool()->push(task);
        //让主线程可以退出循环去join线程
        count++;
        if (count == 6)
            break;
    }

    threadPool<caculate_task>::getThreadPool()->finish();
    return 0;
}