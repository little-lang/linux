#pragma once
#include <iostream>
using namespace std;
enum Flag
{
    TRUE,
    DIVZERO,
    MODZERO
};

string opr = "+-*/%";

class task
{
public:
    task(int x, int y, char opr) : _first(x), _second(y), _opr(opr), _flag(TRUE), _result(0)
    {
    }

    task()
    {}
    void operator()()
    {
        _flag = TRUE;
        switch (_opr)
        {
        case '+':
            _result = _first + _second;
            break;
        case '-':
            _result = _first - _second;
            break;
        case '*':
            _result = _first * _second;
            break;
        case '/':
            if (_second == 0)
                _flag = DIVZERO;
            else
                _result = _first / _second;
            break;
        case '%':
            if (_second == 0)
                _flag = MODZERO;
            else
                _result = _first % _second;
            break;
        }
    }

    string result()
    {
        string str;
        str = to_string(_first) + _opr + to_string(_second) + " = " + to_string(_result) + " flag: [ " + to_string(_flag) + " ]";
        return str;
    }

    string question()
    {
        string str;
        str = to_string(_first) + _opr + to_string(_second) + " = ?";
        return str;
    }

private:
    int _first;
    int _second;
    int _result;
    char _opr;
    int _flag;
};