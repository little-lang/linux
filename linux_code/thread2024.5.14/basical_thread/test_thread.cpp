#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
using namespace std;

//              测试void与void*的大小
// void testVoid()
// {
//     cout << "size of void*: " << sizeof(void *) << endl;
//     cout << "size of void: " << sizeof(void) << endl;
// }

// //                      证明线程是独立的执行流
// void *routine(void *data)
// {
//     //for (int i = 0; i < 5; i++)
//     while(1)
//     {
//         usleep(100000);
//         cout << "线程1, pid: " << getpid() << endl;
//     }
//     return nullptr;
// }

// int main()
// {
//     pthread_t tid;
//     pthread_create(&tid, nullptr, routine, nullptr);
//     //for (int i = 0; i < 7; i++)
//     while(1)
//     {
//         usleep(200000);
//         cout << "线程0, pid: " << getpid() << endl;
//     }
//     return 0;
// }

//                          线程的参数与返回值传递
// struct thread_data
// {
//     string threadName;
//     string threadReturn;
// };

// void *routine(void *data)
// {
//     thread_data *d1 = static_cast<thread_data *>(data);
//     // thread_data *d1 = (thread_data *)(data);
//     int count = 3;
//     for (int i = 0; i < count; i++)
//     {
//         printf("tid: %p threadName: %s count: %d\n", pthread_self(), d1->threadName.c_str(), i);
//         sleep(1);
//     }
//     int a = 5 / 0; // 除0错误 这里说明了当进程中的某个线程出现异常时，整个进程都会退出
//     // exit(0);//使用exit退出 这里也说明了使用exit会退出整个进程
//     d1->threadReturn = "return_" + d1->threadName;
//     return d1;
// }

// void initThread(thread_data *data, int num)
// {
//     data->threadName = "thread_" + to_string(num);
// }

// int main()
// {
//     pthread_t tid;
//     thread_data *data = new thread_data;
//     initThread(data, 1);
//     int ret_create = pthread_create(&tid, nullptr, routine, (void *)data);
//     void *ret_thread;
//     printf("我是主线程tid: %p\n", pthread_self());
//     pthread_join(tid, &ret_thread);
//     cout << ((thread_data *)ret_thread)->threadReturn << endl; // 证明获得了一个类返回值

//     delete data;
//     return 0;
// }