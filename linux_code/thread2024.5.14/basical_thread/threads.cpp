#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <vector>
#include <string>
using namespace std;

struct threadData
{
    string threadName;
    threadData(int num)
    {
        threadName = "thread" + to_string(num);
    }
    threadData() = default;
};

int *g_index;
//int g_val;
__thread int g_val;

void *routine(void *args)
{
    int val = 0;
    threadData *data = (threadData *)args;
    for (int i = 1; i <=3 ; i++)
    {
        //printf("%s tid: %p val: %d\n", data->threadName.c_str(), pthread_self(), val);
        //val++;
        printf("%s g_val: %d\n",data->threadName.c_str(),g_val);
        g_val++;
    }
    // if(data->threadName=="thread1")
    // {
    //     val=10000;
    //     g_index=&val;
    //     sleep(5);
    // }
    return (void *)0;
}

int main()
{
    vector<pthread_t> tids;
    for (int i = 0; i < 3; i++)
    {
        pthread_t tid;
        threadData *td = new threadData(i);
        pthread_create(&tid, nullptr, routine, td);
        tids.push_back(tid);
        sleep(1);
    }
    //cout<<"这是thread2的val值: "<<*g_index<<endl;
    for(auto t:tids)
    {
        void *retData;
        pthread_join(t,&retData);
    }
    return 0;
}