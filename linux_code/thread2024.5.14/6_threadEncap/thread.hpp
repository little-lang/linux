#pragma once
#include <iostream>
#include <string>
#include <pthread.h>
#include<unistd.h>

using namespace std;

typedef void* (*callback)(void *);

class thread
{
public:
    thread()
        : _threadname(""), _isrunning(false)
    {
    }

    thread(callback threadfun, void *args)
        : _threadfun(threadfun), _args(args), _isrunning(false)
    {
        pthread_create(&_tid, nullptr, _threadfun, _args);
        _isrunning = true;
    }

    void join()
    {
        void *ret;
        pthread_join(_tid, &ret);
    }

    bool isrunning()
    {
        return _isrunning;
    }

    pthread_t gettid()
    {
        return _tid;
    }

private:
    
    void *_args;
    string _threadname;
    bool _isrunning;
    pthread_t _tid;
    callback _threadfun;
};