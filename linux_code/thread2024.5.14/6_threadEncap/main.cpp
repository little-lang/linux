#include "thread.hpp"

void *routine(void*args)
{
    cout<<"i am a thread"<<endl;
}

int main()
{
    void *args;
    thread t(routine,&args);
    sleep(1);
    cout<<"thread tid: "<<t.gettid()<<endl;
    cout<<"thread is fun? "<<t.isrunning()<<endl;
    t.join();
    return 0;
}