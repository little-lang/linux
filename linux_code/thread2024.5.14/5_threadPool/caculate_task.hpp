#pragma once
#include <iostream>
#include <unistd.h>
using namespace std;

class caculate_task
{
public:
    caculate_task(int x, char opr, int y)
        : _first(x), _second(y), _opr(opr), _result(0), _erroFlag(0)
    {
    }

    caculate_task() = default;

    void operator()()
    {
        switch (_opr)
        {
        case '+':
            _result = _first + _second;
            break;
        case '-':
            _result = _first - _second;
            break;
        case '*':
            _result = _first * _second;
            break;
        case '/':
            if (_second == 0)
            {
                _erroFlag = 1;
            }
            else
            {
                _result = _first / _second;
            }
            break;
        case ('%'):
            if (_second == 0)
            {
                _erroFlag = 2;
            }
            else
            {
                _result = _first % _second;
            }
            break;
        default:
            _erroFlag = 3;
            break;
        }
    }

    void createTask(const string &name)
    {
        cout << name << " :" << _first << _opr << _second << "= ?" << endl;
    }

    void getAnswer(const string &name)
    {
        cout << name << ": " << _first << _opr << _second << "=" << _result << " flag: " << _erroFlag << endl;
    }

private:
    int _first;
    int _second;
    char _opr;
    int _erroFlag;
    double _result;
};