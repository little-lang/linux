#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <unistd.h>
#include <queue>
#include <pthread.h>
#include <sys/types.h>
using namespace std;

#define THREADNUM 5

template <class T>
class threadPool
{
public:
private:
    static void *routine(void *args)
    {
        threadPool<T> *pool = static_cast<threadPool<T> *>(args);
        T task;
        while (1)
        {
            pool->pop();
        }
    }

public:
    threadPool(int num = THREADNUM)
    {
        pthread_mutex_init(&_mutex, nullptr);
        pthread_cond_init(&_cond, nullptr);
        _info.resize(THREADNUM);
    }

    void start()
    {
        for (int i = 0; i < THREADNUM; i++)
        {
            pthread_create(&(_info[i]._tid), nullptr, routine, (void *)this);
            _info[i]._threadName = "thread" + to_string(i);
        }
    }

    void finish()
    {
        void *ret;
        for (auto tinfo : _info)
        {
            pthread_join(tinfo._tid, &ret);
        }
    }

    void push(const T &task) // 这里生产任务只需要一直生产即可，不需要访问控制
    {
        pthread_mutex_lock(&_mutex);
        _task.push(task);
        pthread_cond_signal(&_cond);
        pthread_mutex_unlock(&_mutex);
    }

    void pop()
    {
        pthread_mutex_lock(&_mutex);
        while (_task.empty())
        {
            pthread_cond_wait(&_cond, &_mutex);
        }
        T top = _task.front();
        _task.pop();
        top();
        top.getAnswer(getThreadName(pthread_self()));
        pthread_mutex_unlock(&_mutex);
    }

    string getThreadName(pthread_t tid)
    {
        for (auto info : _info)
        {
            if (info._tid == tid)
                return info._threadName;
        }
        return "no thred";
    }

    int tasknum()
    {
        return _task.size();
    }

private:
    struct threadInfo
    {
        string _threadName;
        pthread_t _tid;
    };

    pthread_mutex_t _mutex;
    pthread_cond_t _cond;
    queue<T> _task;
    vector<threadInfo> _info;
};