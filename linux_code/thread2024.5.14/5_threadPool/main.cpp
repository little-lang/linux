#include<iostream>
#include"threadpool.hpp"
#include"caculate_task.hpp"
#include<ctime>
using namespace std;


int main()
{
    threadPool<caculate_task> pool;
    srand((unsigned int)time(nullptr));
    char opr[]="+-*/%";
    //启动线程池,线程池等待任务产生处理任务
    pool.start();
    while(1)
    {
        sleep(1);
        //主线程生产任务
        int x, y;
        x=rand()%10;
        usleep(10);
        y=rand()%10;
        char op=opr[rand()%5];
        caculate_task task(x,op,y);
        pool.push(task);
    }


    return 0;
}