#pragma once
#include<pthread.h>


class Mutex
{
private:
    pthread_mutex_t _mutex;

public:
    Mutex()
    {
        pthread_mutex_init(&_mutex, nullptr);
    }
    ~Mutex()
    {
        pthread_mutex_destroy(&_mutex);
    }
    void lock()
    {
        pthread_mutex_lock(&_mutex);
    }
    void unlock()
    {
        pthread_mutex_unlock(&_mutex);
    }
};

class lockGuard
{
public:
    lockGuard(Mutex *mutex)
        : _mutex(mutex)
    {
        _mutex->lock();
    }
    ~lockGuard()
    {
        _mutex->unlock();
    }

private:
    Mutex *_mutex;
};