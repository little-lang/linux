#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include<cstring>
using namespace std;

void* routine(void*args)
{
    cout<<"我是被创建线程"<<endl;
}

int main()
{
    pthread_t tid;
    pthread_create(&tid,nullptr,routine,nullptr);
    void* ret;
    pthread_detach(tid);
    int ret_join=pthread_join(tid,&ret);
    printf("%s\n",strerror(ret_join));
    return 0;
}