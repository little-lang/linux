#include <iostream>
#include <vector>
#include <pthread.h>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include"RAII_mutex.h"
using namespace std;



struct threadData
{
    string _threadName;
    Mutex *_mutex;
    threadData(int num, Mutex *mutex)
        : _threadName("thread" + to_string(num)), _mutex(mutex)
    {
    }
    threadData() = default;
};

void *routine(void *args)
{
    threadData *data = static_cast<threadData *>(args);
    {
        lockGuard lock(data->_mutex);
        for (int i = 0; i < 5; i++)
        {
            printf("I am %s\n", data->_threadName.c_str());
        }
        sleep(1);
    }
}

int main()
{
    pthread_t tid;
    Mutex mutex;

    for(int i=0;i<3;i++)
    {
        threadData *data = new threadData(i, &mutex);
        pthread_create(&tid, nullptr, routine, data);
    }

    void *retData;
    pthread_join(tid, &retData);

    return 0;
}