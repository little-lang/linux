#include<stdio.h>
#include<sys/types.h>
#include<unistd.h>
#include<wait.h>
#include<stdlib.h>

int main()
{
  printf("这是一个测试wait的程序\n");
  pid_t ret=fork();
  if(ret==0)
  {
    printf("这是子进程\n");
    int count=5;
    while(count)
    {
      printf("倒计时：%d\n",count);
      count--;
    }
    exit(-1);
  }
  else if(ret>0)
  {
    wait(NULL);
    printf("我一定会等待5秒后才出现\n");
  }
  return 0;
}
