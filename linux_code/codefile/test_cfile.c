#include<stdio.h>
//对fgets函数的测试事实证明了fgets函数会在获取数据后在数组末尾自动加上\0
//所以带s这样的函数我们可以理解为字符串函数它们一般都是会在数组末尾自动加\0的

int main()
{
  FILE*fp=fopen("tmp.txt","r");
  if(!fp)
  {
    perror("fopen");
    printf("fopen错误\n");
    return 1;
  }
  char line[3];
  while(fgets(line,sizeof(line),fp))
  {
    fprintf(stdout,"%s",line);
  }
  return 0;
}
