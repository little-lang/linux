#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<iostream>
#include<vector>
#include<stdlib.h>

typedef void(*function)();
std::vector<function> load_vector;

void fun1()
{
  printf("这是要做的事情1\n");
}

void fun2()
{
  printf("这是要做的事情2\n");
}

void fun3()
{
  printf("这是要做的事情3\n");
}

void load()//当要在非阻塞等待时执行任务只需要在这个函数中注册即可
{
    load_vector.push_back(fun1);
    load_vector.push_back(fun2);
    load_vector.push_back(fun3);
    sleep(2);
}

int main()
{
  pid_t ret=fork();
  if(ret==0)
  {
    //这是子进程
      printf("我是子进程我的进程id是: %d我正在运行\n",getpid());
      //休息5秒让父进程的现象显示出来
      sleep(5);
      //告诉用户子进程还在运行
      printf("我是子进程我仍然还在运行\n");
      exit(128);
  }
  else if(ret>0)
  {
    //这是父进程
    int status=0;
    while(waitpid(-1,&status,WNOHANG)==0)
    {
      load();//注册我们的行为
      for(int i=0;load_vector[i];i++)//执行表中的行为
      {
        load_vector[i]();
      }
    }
    printf("获得返回值: %d\n",WEXITSTATUS(status));
  }
  return 0;
}
