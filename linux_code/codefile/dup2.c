#include<stdio.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<unistd.h>

//使用dup2系统调用来实现重定向

int main()
{
  //文件描述符fd
  int fd=open("tmp.txt",O_WRONLY|O_CREAT|O_APPEND);
  if(fd<0)
  {
    perror("open");
    return 1;
  }
  dup2(fd,1);
  char str[]="aaaaabbbbbbccccc";
  printf("%s\n",str);
  return 0;
}
