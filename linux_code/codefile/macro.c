#include<stdio.h>
//通过宏标志位来传递参数
#define FIRST 0x1
#define SECOND 0x2
#define THIRD 0x4

void fun(int macro)
{
  if(macro&FIRST)
    printf("这是功能1\n");
  if(macro&SECOND)
    printf("这是功能2\n");
  if(macro&THIRD)
    printf("这是功能3\n");
}

int main()
{
  fun(1);//0001
  printf("-------------------\n");
  fun(2);//0010
  printf("-------------------\n");
  fun(3);//0011
  printf("-------------------\n");
  fun(4);//0100
  printf("-------------------\n");
  fun(5);//0101

  return 0;
}
