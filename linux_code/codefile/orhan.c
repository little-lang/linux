//模拟孤儿进程代码

#include<stdlib.h>   
#include<stdio.h>   
#include<unistd.h>                                   
int main()
{     
 int ret=fork();
 if(ret>0)      
 {        
   printf("I am father :pid%d\n",getpid());
   sleep(1);                  
   exit(0);                 
 }        
 else if(ret==0)
 {          
   printf("I am child :pid=%d\n",getpid());
   sleep(5);                  
 }                      
 return 0;   
}  
