//数据所在虚拟地址空间位置代码
#include<stdio.h>
#include<stdlib.h>

int g_data=10;
int uninit_g_data;


int main(int argc,char*argv[],char*env[])
{
  printf("代码段地址（main函数也是代码）:%p\n",main);
  char *str= "abcde";
  static s_val=1;
  printf("常量数据地址:%p\n",str);
  printf("已初始化全局变量地址:%p\n",&g_data);
  printf("未初始化全局变量地址:%p\n",&uninit_g_data);
  printf("静态数据地址:%p\n",&s_val);
  int *val=(int*)malloc(sizeof(4));
  int *val1=(int*)malloc(sizeof(4));
  int *val2=(int*)malloc(sizeof(4));
  int *val3=(int*)malloc(sizeof(4));
  printf("堆区数据1地址:%p\n",val);
  printf("堆区数据2地址:%p\n",val1);
  printf("堆区数据3地址:%p\n",val2);
  printf("堆区数据4地址:%p\n",val3);
  printf("栈区数据1地址:%p\n",&val);
  printf("栈区数据2地址:%p\n",&val1);
  printf("栈区数据3地址:%p\n",&val2);
  printf("栈区数据4地址:%p\n",&val3);
  printf("命令行参数地址:%p\n",argv[0]);
  printf("全局环境变量参数地址:%p\n",env[0]);

  return 0;
}
