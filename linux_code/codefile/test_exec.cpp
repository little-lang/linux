//测试exec的6个函数
  #include<stdio.h>
  #include<unistd.h>
  #include<sys/types.h>
  #include<sys/wait.h>
  
  int main()
  {
    extern char**environ;
    char *myenv[3]={"one_env","two_env"};
    pid_t ret=fork();
    if(ret==0)
    {
      //子进程
      //进程替换
      char * str[5]={"./tmp"};
      //execv("/usr/bin/ls",str);
      //execl("/usr/bin/ls","ls","-l","-a","--color=auto",NULL);
      //execvp("ls",str);
      //execlp("ls","ls","-l","-a","--color=auto",NULL);
      
      execvpe("./tmp",str,myenv);
      //execle("/usr/bin/ls","ls","-l","-a","--color=auto",NULL,myenv);
    } 
    else if(ret>0)
    {
      //父进程
      wait(NULL);
      printf("wait success\n");
    }
    return 0;
  }

