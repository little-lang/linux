#include<stdio.h>    
int main(int argc,char*argv[],char*env[])    
{   
  int i=0;    
  while(env[i]!=NULL)    
  {    
    printf("env[%d]:%s\n",i,env[i]);    
    i++;     
  }                                                                                 
  return 0;                                                                                                            
}

//方法2
/*#include<stdio.h>
int main()
{
    extern char**environ;
     int i;
    for(i=0;environ[i]!=NULL;i++)
    {
      printf("environ[%d]:%s\n",i,environ[i]);
    }
    return 0;
}*/


//方法3
/*#include<stdio.h>    
#include<stdlib.h>
int main()    
{    
    printf("environ:%s\n",getenv("PATH"));       
    return 0;    
}*/
