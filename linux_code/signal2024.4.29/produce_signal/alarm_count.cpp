#include <iostream>
#include <unistd.h>
#include <signal.h>
using namespace std;

static int count=0;

void handler(int signum)
{
    cout<<"count="<<count<<endl;
    alarm(1);
}

int main()
{
    alarm(1);
    signal(14, handler);
    while (true)
    {
        count++;
    }
    return 0;
}