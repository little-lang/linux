#include<iostream>
#include<unistd.h>
#include<sys/types.h>
#include<signal.h>
using namespace std;

void catchsig(int signalnum)
{
    cout<<"catch signalnum is: "<<signalnum<<endl;
    exit(1);//读取信号后退出
}

int main()
{
    signal(SIGINT,catchsig);
    while(1)
    {
        cout<<getpid()<<": 我正在运行"<<endl;
        sleep(1);
    }
    return 0;
}