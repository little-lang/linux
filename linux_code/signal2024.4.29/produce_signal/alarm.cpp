#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <ctime>
#include <functional>
#include <vector>
using namespace std;

// typedef void(*func)();
typedef function<void()> func;

vector<func> task_table;

void handler(int signum)
{
    cout << "catch signal: " << signum << endl;
    for (auto &func : task_table)
    {
        func();
    }
    alarm(1);
}
void log()
{
    time_t curtime = time(nullptr);
    cout << asctime(localtime(&curtime)) << "打印日志信息" << endl;
}

void exam()
{
    cout << "检查一下程序" << endl;
}

void load()
{
    task_table.push_back(log);
    task_table.push_back(exam);
}

int main()
{
    alarm(1);
    load(); // 加载任务列表
    signal(14, handler);
    int count = 0;
    while (true)
    {
        cout << getpid() << ": 正在运行中" << count++ << endl;
        sleep(1);
    }
    return 0;
}