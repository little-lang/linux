// 硬件异常产生信号

#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <cassert>
#include <sys/wait.h>
#include<signal.h>
using namespace std;

void handler (int signum)
{
    cout<<"catch signal : "<<signum<<endl;
    sleep(1);
}

int main()
{
    pid_t pid = fork();
    assert(pid != -1);
    if (pid == 0) // 子进程
    {
        signal(SIGFPE,handler);
        int a = 10;
        a /= 0;
    }
    wait(nullptr);
    // int status=0;
    // waitpid(pid,&status,0);
    // cout<<WTERMSIG(status)<<endl;
    return 0;
}