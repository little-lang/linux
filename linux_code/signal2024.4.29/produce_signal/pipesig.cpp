#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <cassert>
#include <sys/wait.h>
#include<signal.h>
using namespace std;

void hander(int signum)
{
    cout<<"catch signal : "<<signum<<endl;
}

int main()
{
    int pipefd[2];
    int ret_pipe = pipe(pipefd);
    assert(ret_pipe != -1);
    (void)ret_pipe;
    pid_t pid = fork();
    if (pid == 0) // 子进程
    {
        close(pipefd[0]);
        signal(13,hander);
        int i=0;
        while (true) // 死循环 永远不会退出
        {
            cout << getpid() << ": 我是子进程，我正在发消息" << i++ << endl;
            int tem = 1;
            write(pipefd[1], &tem, sizeof(tem));
            sleep(1);
        }
        exit(1);
    }
    close(pipefd[1]); // 关闭写管道
    for (int i = 0; i < 5; i++)
    {
        cout << getpid() << ": 父进程进程正在运行" << endl;
        sleep(1);
    }
    close(pipefd[0]);
    cout << "父进程读管道关闭成功" << endl;
    for (int i = 0; i < 5; i++)
    {
        cout << getpid() << ": 父进程正在运行" << endl;
        sleep(1);
    }

    wait(nullptr);
    return 0;
}