#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
using namespace std;

void handler(int signum)
{
     cout<<getpid()<<": catch signal "<<signum<<endl;
}

void handler1()
{
    sigset_t pending;
    sigpending(&pending);
    for (int i = 31; i > 0; i--)
    {
        int ans = sigismember(&pending, i);
        cout << ans;
    }
    cout << endl;
    sleep(1);
}

int main()
{
    sigset_t set;
    sigemptyset(&set);           // 初始化将要替换block的set
    // for (int i = 31; i > 0; i--) // 打印一下初始化的set
    // {
    //     int ans = sigismember(&set, i);
    //     cout << ans;
    // }
    // cout << endl;
    for (int signum = 1; signum <= 31; signum++)
    {
        //signal(signum,handler);//将1到31号信号全部捕捉发现9号信号无法被捕捉

        sigaddset(&set, signum);     // 添加信号进入set
        // for (int i = 31; i > 0; i--) // 打印一下修改的set
        // {
        //     int ans = sigismember(&set, i);
        //     cout << ans;
        // }
        // cout << endl;
        sigprocmask(SIG_SETMASK, &set, nullptr); // 成功将信号屏蔽
    }

    while (true) // 让进程死循环防止退出
    {
        handler1();
    }

    return 0;
}
