#include <iostream>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
using namespace std;

// 证明一个信号在被执行的时候它的信号集会被屏蔽接os无法去处理任何其他信号
void showpending()
{
    sigset_t pending;
    sigpending(&pending);
    cout << "信号集: ";
    for (int i = 31; i > 0; i--)
    {
        int ans = sigismember(&pending, i);
        cout << ans;
    }
    cout << endl;
}

void handler(int signum)
{
    cout << getpid() << ": catch signal " << signum << endl;
    for (int i = 0; i < 5; i++)
    {
        showpending();
        sleep(1);
    }
}

int main()
{
    sigset_t set;
    signal(2, handler);
    showpending();
    while (true)
    {
    }
    return 0;
}