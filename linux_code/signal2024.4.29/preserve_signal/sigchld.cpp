#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <signal.h>
using namespace std;

void handler(int signum)
{
    pid_t pid;
    while ((pid = waitpid(-1, nullptr, WNOHANG)) > 0)
    {
        cout <<"wait success : "<<pid << endl;
    }
}

int main()
{
    signal(SIGCHLD, handler);
    // signal(17,handler);
    for (int i = 0; i < 5; i++) // 创建5个进程
    {
        pid_t pid = fork();
        if (pid == 0)
        {
            sleep(10);
            exit(0);
        }
    }
    for (int i = 0; i < 12; i++)
    {
        cout << "父进程正在运行 " << i << endl;
        sleep(1);
    }
    return 0;
}