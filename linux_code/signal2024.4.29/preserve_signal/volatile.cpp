#include<iostream>
#include<unistd.h>
#include<signal.h>
using namespace std;

int count=0;

void handler(int signum)
{
    cout<<getpid()<<": catch signal "<<signum<<" change count->1"<<endl;
    count=1;
}

int main()
{
    signal(2,handler);
    while(count==0){}
    cout<<"成功退出"<<endl;
    return 0;
}