#pragma once

#include <iostream>

#include "socket.hpp"

class tcpserver
{
public:
    tcpserver()
    {
    }

    ~tcpserver()
    {
    }

    void init()
    {
        _listenSock.Bind(_port);
        _listenSock.Listen();
    }

    void run()
    {
        while (true)
        {
            string clientIp;
            uint16_t clientPort;
            int serverSock = _listenSock.Accept(clientIp, &clientPort);
            if (serverSock == -1)
                continue;
        }
    }

private:
    uint16_t _port;
    Socket _listenSock;
};