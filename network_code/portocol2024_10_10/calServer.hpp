#pragma once

#include <iostream>
#include <string>
#include "protocol.hpp"

enum calError
{
    DEVZERO,
    MODZERO,
    OTHEROP
};

class calserver
{
public:
    calserver()
    {
    }
    ~calserver()
    {
    }

    response headle(const request &req)
    {
        response res;
        switch (req._op)
        {
        case '+':
            res._result = req._x + req._y;
            break;
        case '-':
            res._result = req._x - req._y;
            break;
        case '*':
            res._result = req._x * req._y;
            break;
        case '/':
            if (req._y == 0)
            {
                res._code = DEVZERO;
                break;
            }
            res._result = req._x / req._y;
            break;
        case '%':
            if (req._y == 0)
            {
                res._code = MODZERO;
                break;
            }
            res._result = req._x % req._y;
            break;
        default:
            res._code = OTHEROP;
            break;
        }
        return res;
    }

    std::string caculate(std::string& massge)
    {
        std::string data;
        if (!unpack(massge, data))
        {
            // lg(DEBUG, "line67");
            return "";
        }
        // 将获取到的序列化的数据解包后获得data,再将data进行反序列化
        request req;
        if (!req.deserialize(data))
        {
            // lg(DEBUG, "line74");
            return "";
        }
        // data反序列化成功后处理数据
        response res = headle(req);
        // 处理数据成功后将数据序列化与打包
        data="";

        res.serialize(data);

        res.getRes();//输出成功计算的数据

        encap(data);


        return data;
    }

private:
};