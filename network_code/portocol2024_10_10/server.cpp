#include "protocol.hpp"
#include"tcpServer.hpp"

void test()
{
    //测试res
    request res(1234567, '+', 89101112);
    std::string massge;
    res.serialize(massge);
    std::cout << massge << std::endl;
    encap(massge);
    std::cout << massge;

    std::string data;
    unpack(massge, data);
    std::cout << data << std::endl;

    request tmp;
    tmp.deserialize(data);
    std::cout << tmp._x << std::endl;
    std::cout << tmp._op << std::endl;
    std::cout << tmp._y << std::endl;

    //测试response
    // response rep(10000, 0);
    // std::string massage;
    // rep.serialize(massage);
    // std::cout << massage << std::endl;

    // encap(massage);
    // std::cout << massage;

    // std::string data;
    // unpack(massage, data);
    // std::cout << data << std::endl;

    // response tmp;
    // tmp.deserialize(data);
    // std::cout << tmp._result << std::endl;
    // std::cout << tmp._code << std::endl;
}

int main(int argc,char*argv[])
{
    if(argc!=2)
    {
        lg(FATAL,"argc should be 2:  %s port",argv[0]);
        return 0;
    }

    tcpserver ser(atoi(argv[1]));
    ser.run();
    

    return 0;
}