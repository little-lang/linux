
#include "client.hpp"

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        lg(FATAL, "argc should be 3:  %s \"ip\" \"prot\" ", argv[0]);
        exit(-1);
    }
    std::string ip(argv[1]);
    uint16_t port = atoi(argv[2]);
    tcpClient cli(ip,port);
    cli.run();

    return 0;
}