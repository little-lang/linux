#include"server.hpp"

string chat(const string& buffer)
{
    string info = "get message: ";
    info += buffer;
    //cout << info << endl;
    return info;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        log(ERROR,"argc should be 2");
        exit(-1);
    }
    uint16_t port=stoi(argv[1]);
    unique_ptr<udpserver> ptr(new udpserver(port));
    ptr->run();

    return 0;
}