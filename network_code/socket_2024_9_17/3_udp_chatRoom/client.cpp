#include"client.hpp"


int main(int argc,char* argv[])
{
    if(argc!=3)
    {
        log(ERROR,"argc should be 3");
        exit(-1);
    }
    string ip=argv[1];
    uint16_t port =stoi(argv[2]);
    unique_ptr<client> ptr(new client(ip,port));
    ptr->run();


    return 0;
}