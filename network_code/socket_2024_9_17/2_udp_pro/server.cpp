#include"server.hpp"

string addStr(const string& buffer)
{
    string info = "get message: ";
    info += buffer;
    cout << info << endl;
    return info;
}

bool checksafe(const string& comd)
{
    vector<string> v={
        "rm"
    };
    for(auto word:v)
    {
        if(comd.find(word)!=string::npos)
        {
            return true;
        }
    }
    return false;
}

string command(const string& comd)
{
    cout<<comd<<endl;
    if(checksafe(comd))
    {
        return "command not safe";
    }
    FILE*f=popen(comd.c_str(),"r");
    if(f==nullptr)
    {
        cout<<strerror(errno)<<endl;
        exit(-1);
    }
    string back_info;
    while(true)
    {
        char buffer[4096]={0};
        char * ret=fgets(buffer,sizeof(buffer),f);
        if(ret==nullptr)break;
        back_info+=buffer;
    }
    return back_info;
}



int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        log(ERROR,"argc should be 2");
        exit(-1);
    }
    uint16_t port=stoi(argv[1]);
    unique_ptr<udpserver> ptr(new udpserver(port));
    ptr->run(addStr);

    return 0;
}