#pragma once

#include <iostream>
#include <ctime>

using namespace std;

enum log_num
{
    DEBUG,
    NOTICE,
    WARNING,
    ERROR
};

string log_message[]{
    "debug",
    "notice",
    "worning",
    "error"};

void log(int num, const char *message)
{
    time_t curtime = time(nullptr);
    cout << asctime(localtime(&curtime));
    cout << log_message[num] << " | " << message << endl;
}