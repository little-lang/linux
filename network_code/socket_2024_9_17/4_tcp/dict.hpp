#pragma once
#include <iostream>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "log.hpp"
#include <cstring>
#include <unistd.h>
#include <unordered_map>
#include <fstream>

class dict
{
public:
    void Init()
    {
        ifstream in("./dict.txt");
        string str;
        while (getline(in, str))
        {
            string key, value;
            auto pos = str.find(':');
            if (pos == string::npos)
                continue;
            key = str.substr(0, pos);
            value = str.substr(pos + 1);
            _dict.insert({key, value});
        }
    }

    static dict *getdict()
    {
        // 这里因为是只读，不会对其修改，所以不需要上锁
        if (_self == nullptr)
        {
            _self = new dict;
        }
        return _self;
    }

    unordered_map<string, string> _dict;

private:
    dict() {}
    dict(const dict &) = delete;
    dict &operator=(const dict &) = delete;

    static dict *_self;
};

dict* dict::_self = nullptr;