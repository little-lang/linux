#include"server.hpp"
#include<memory>
#include"dict.hpp"
#include"daemon.hpp"


int main(int argc,char *argv[])
{
    //signal(SIGPIPE,SIG_IGN); //对服务器进程进行保护，让服务器不会被信号杀掉
    daemon();
    dict::getdict()->Init();
    if(argc != 2)
    {
        cout<<"argc should be 2"<<endl;
        exit(-1);
    }
    uint16_t port=atoi(argv[1]);
    unique_ptr<tcpServer> ptr(new tcpServer(port));
    ptr->run();

    return 0;
}