#include"client.hpp"

int main(int argc,char* argv[])
{
    if(argc != 3)
    {
        cout<<"argc should be 3"<<endl;
        exit(-1);
    }
    string serverIp=argv[1];
    uint16_t serverPort=stoi(argv[2]);

    tcpclient client(serverIp,serverPort);
    client.run();

    return 0;
}