#pragma once

#include<iostream>
#include<unistd.h>
#include<sys/types.h>
#include<signal.h>
#include<sys/stat.h>
#include<fcntl.h>

void daemon()
{
    if(fork()!=0)exit(-1);//父进程直接退出   

    //忽略信号，让信号不会误杀进程
    signal(SIGPIPE,SIG_IGN);
    signal(SIGCLD,SIG_IGN);
    signal(SIGSTOP,SIG_IGN);

    setsid();//自成会话

    //更改pwd
    //chdir("/");

    //重定向012fd到/dev/null 堵不如疏
    int fd=open("/dev/null",O_RDWR);
    dup2(fd,0);
    dup2(fd,1);
    dup2(fd,2);

}