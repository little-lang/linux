#include <iostream>
#include <string>
#include <sstream>

void useString(std::string str, const std::string op = " ")
{
    while (true)
    {
        int pos = str.find(op);
        if (pos == std::string::npos)
        {
            break;
        }
        std::cout << str.substr(0, pos) << " ";
        str.erase(0, pos + 1);
    }
    if (!str.empty())
        std::cout << str;
    std::cout << std::endl;
}

void useStrstream(std::string str, const char op = ' ')
{
    std::stringstream s(str);
    std::string tmp;
    while (std::getline(s, tmp, op))
    {
        std::cout << tmp << " ";
    }
    std::cout << std::endl;
}

int main()
{
    std::string s("apple banana cat dog");
    std::string s1("apple?banana?cat?dog");
    std::string s2("applebananacatdog");
    useString(s, " ");
    useStrstream(s, ' ');
    useString(s1, "?");
    useStrstream(s1, '?');
    //测试一下stringstream在没有遇到分隔符时是否还会读取
    useStrstream(s2, '?');
    return 0;
}