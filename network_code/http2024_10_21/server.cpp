#include"server.hpp"

int main(int argc,char *argv[])
{
    if(argc!=2)
    {
        lg(FATAL,"should intput \"%s port\"",argv[0]);
        exit(-1);
    }
    uint16_t port=atoi(argv[1]);
    
    server s(port);
    s.run();

    return 0;
}